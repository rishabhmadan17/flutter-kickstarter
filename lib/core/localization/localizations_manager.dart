import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'app_localizations.dart';

//To add a locale simply create the file and change the supportedLocales list
//below

///List of the locales supported by the app.
///The first element of this list will be the **default** one so it better be
///english :)
List<Locale> supportedLocales = const [
  Locale("en", "GB"),
  Locale("it", "IT"),
];

List<LocalizationsDelegate> delegates = [
  // Loads the translation from the json files in /lang
  AppLocalizations.delegate,
  // Built-in localization of bacis text for Material widgets
  GlobalMaterialLocalizations.delegate,
  // Built-in localization for text direction RTL/LTR
  GlobalWidgetsLocalizations.delegate,
];

///This is the function that chooses which locale to use in the app based on
///what the user has requested
Locale chooseLocaleForThisApp(Locale requested, Iterable<Locale> supported) {
  // Check if the device locale is supported for that language and region
  for (final supportedLocale in supportedLocales) {
    if (supportedLocale.languageCode == requested.languageCode &&
        supportedLocale.countryCode == requested.countryCode) {
      return supportedLocale;
    }
  }
  // Check if at least the language is supported
  for (final supportedLocale in supportedLocales) {
    if (supportedLocale.languageCode == requested.languageCode) {
      return supportedLocale;
    }
  }
  // If the locale of the device is not supported, use the first one
  // from the list (English, in this case).
  return supportedLocales.first;
}
